package com.ljph.seckill.common;

import com.jfinal.config.*;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.json.FastJsonFactory;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.ljph.seckill.controller.CommonController;
import com.ljph.seckill.controller.SeckillController;
import com.ljph.seckill.model._MappingKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by yuzhou on 16/9/1.
 */
public class MyConfig extends JFinalConfig {

    private static final Logger _log = LoggerFactory.getLogger(MyConfig.class);

    public MyConfig(){
        _log.debug("MyConfig for JFinal constructed ...");
    }

    @Override
    public void configConstant(Constants constants) {

        constants.setDevMode(true);

        // 默认后端模板路径
        constants.setBaseViewPath("/view/ftl");
        constants.setFreeMarkerViewExtension("ftl");

        // 默认使用Fastjson来渲染数据
        constants.setJsonFactory(new FastJsonFactory());
    }

    @Override
    public void configRoute(Routes routes) {
        routes.add("/", CommonController.class);
        routes.add("/seckill", SeckillController.class, "seckill");
    }

    @Override
    public void configPlugin(Plugins plugins) {

        // 配置C3p0数据源插件
        Prop p = PropKit.use("config/mysql.properties");
        C3p0Plugin c3p0Plugin = new C3p0Plugin(p.getProperties());
        c3p0Plugin.start();

        // 配置ActiveRecord 数据访问插件
        ActiveRecordPlugin arp = new ActiveRecordPlugin("testDs", c3p0Plugin);
        _MappingKit.mapping(arp);
        arp.setDevMode(true);
        arp.setShowSql(true);
        arp.start();
    }

    @Override
    public void configInterceptor(Interceptors interceptors) {

    }

    @Override
    public void configHandler(Handlers handlers) {
        handlers.add(new ContextPathHandler("ctx"));
    }

    @Override
    public void afterJFinalStart() {
        _log.debug("JFinal started successfully ...");
    }

    @Override
    public void beforeJFinalStop() {
        _log.debug("JFinal will be stopped ...");
    }
}
